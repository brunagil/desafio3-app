export enum ECategorias {
    'Transporte' = 1,
    'Compras Online',
    'Saúde e Beleza',
    'Serviços Automotivos',
    'Restaurantes',
    'Super Mercados'
}
