import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { GastosGeraisModel } from '../../models/gastos-gerais/gastos-gerais';
import { CategoriasModel } from '../../models/categorias/categorias';


@Injectable({
  providedIn: 'root'
})
export class LancamentosService {

  constructor(
    private httpClient: HttpClient
  ) { }

  obterLancamentos() {
    return this.httpClient.get<GastosGeraisModel[]>(`${environment._URL}/lancamentos`);
  }

  obterPorCategoria() {
    return this.httpClient.get<CategoriasModel[]>(`${environment._URL}/categorias`);
  }
}
