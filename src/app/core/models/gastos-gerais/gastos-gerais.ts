export class GastosGeraisModel  {
    id: number;
    valor: number;
    categoria: number;
    origem: string;
    mes_lancamento: number;
}
