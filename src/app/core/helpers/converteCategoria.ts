import { ECategorias } from '../enums/categoria/categorias.enum';

export const converteCategoria = (num: string): string => {
    return ECategorias[num];
};
