import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';

import { HomeComponent } from './pages/home/home.component';
import { GastosGeraisComponent } from './components/gastos-gerais/gastos-gerais.component';
import { GastosPorCategoriaComponent } from './components/gastos-por-categoria/gastos-por-categoria.component';
import { GastosPorMesComponent } from './components/gastos-por-mes/gastos-por-mes.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GastosGeraisComponent,
    GastosPorMesComponent,
    GastosPorCategoriaComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
