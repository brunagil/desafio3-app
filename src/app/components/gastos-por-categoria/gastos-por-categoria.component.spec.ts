import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GastosPorCategoriaComponent } from './gastos-por-categoria.component';

describe('GastosPorCategoriaComponent', () => {
  let component: GastosPorCategoriaComponent;
  let fixture: ComponentFixture<GastosPorCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GastosPorCategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastosPorCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
