import { Component, OnInit } from '@angular/core';

import { LancamentosService } from 'src/app/core/services/lancamentos/lancamentos.service';

import { converteCategoria } from 'src/app/core/helpers/converteCategoria';

@Component({
  selector: 'app-gastos-por-categoria',
  templateUrl: './gastos-por-categoria.component.html',
  styleUrls: ['./gastos-por-categoria.component.css']
})
export class GastosPorCategoriaComponent implements OnInit {
  dataSourceCategoria: any;
  gastosPorCategoria: any[];

  constructor(
    private gastosGerais: LancamentosService
  ) { }

  ngOnInit(): void {
    this.geraConsolidadoPorCategoria();
  }

  geraConsolidadoPorCategoria() {
    this.gastosGerais.obterLancamentos()
      .subscribe((resp: any) => {

        let dataSourceCategoria = {};

        resp.forEach(element => {
          dataSourceCategoria = {
            ...dataSourceCategoria,
            [element.categoria]:
              ((dataSourceCategoria[element.categoria] === undefined ? 0 : dataSourceCategoria[element.categoria])
                + element.valor
              )

          };
        });
        this.dataSourceCategoria = dataSourceCategoria;

        const arr = [];
        for (const key of Object.keys(dataSourceCategoria)) {
          const categoria = converteCategoria(key);
          arr.push({
            categoria,
            valor: dataSourceCategoria[key]
          });
        }
        this.gastosPorCategoria = arr;
      });
  }
}
