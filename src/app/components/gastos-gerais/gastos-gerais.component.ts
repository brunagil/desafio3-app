import { Component, OnInit } from '@angular/core';

import { LancamentosService } from 'src/app/core/services/lancamentos/lancamentos.service';

import { GastosGeraisModel } from 'src/app/core/models/gastos-gerais/gastos-gerais';

@Component({
  selector: 'app-gastos-gerais',
  templateUrl: './gastos-gerais.component.html',
  styleUrls: ['./gastos-gerais.component.css']
})
export class GastosGeraisComponent implements OnInit {
  listaDeGastos: GastosGeraisModel[];

  constructor(
    private gastosGerais: LancamentosService
  ) { }

  ngOnInit(): void {
    this.geraListaDeGastos();
  }

  geraListaDeGastos() {
    this.gastosGerais.obterLancamentos()
      .subscribe((resp: any) => {

        const listaDeGastos = [];

        resp.forEach((element: { valor: any; origem: any; }) => {
          listaDeGastos.push({
            valor: element.valor,
            origem: element.origem,
          });
        });
        this.listaDeGastos = listaDeGastos;
      });
  }
}
