import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GastosPorMesComponent } from './gastos-por-mes.component';

describe('GastosPorMesComponent', () => {
  let component: GastosPorMesComponent;
  let fixture: ComponentFixture<GastosPorMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GastosPorMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastosPorMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
