import { Component, OnInit } from '@angular/core';
import { EMesesDoAno } from 'src/app/core/enums/meses/meses.enum';
import { LancamentosService } from 'src/app/core/services/lancamentos/lancamentos.service';
import { GastosMensaisModel } from 'src/app/core/models/gastos-mensais/gastos-mensais';
import { converteMeses } from 'src/app/core/helpers/converteMeses';

@Component({
  selector: 'app-gastos-por-mes',
  templateUrl: './gastos-por-mes.component.html',
  styleUrls: ['./gastos-por-mes.component.css']
})
export class GastosPorMesComponent implements OnInit {
  dataSourceMensal: {};
  dataSourceConsolidada: GastosMensaisModel[];
  gastosMensaisColumns: string[] = ['mes', 'valorTotal'];

  constructor(
    private gastosMensais: LancamentosService
  ) { }

  ngOnInit(): void {
    this.geraConsolidado();
  }

  geraConsolidado() {
    this.gastosMensais.obterLancamentos()
      .subscribe((resp: any) => {

        let dataSourceMensal = {};

        resp.forEach(element => {
          dataSourceMensal = {
            ...dataSourceMensal,
            [element.mes_lancamento]:
              ((dataSourceMensal[element.mes_lancamento] === undefined ? 0 : dataSourceMensal[element.mes_lancamento])
                + element.valor
              )
          };
        });
        this.dataSourceMensal = dataSourceMensal;

        const arr = [];
        for (const key of Object.keys(dataSourceMensal)) {
          const mes = converteMeses(key);
          arr.push({
            mes,
            valorTotal: dataSourceMensal[key]
          });
        }
        this.dataSourceConsolidada = arr;
      });
  }
}
