## Histórico Fatura - App JavaScript

Chegamos ao fim! :) Abaixo estão algumas informações sobre o projeto:

### Cenário Proposto
_Usando a mesma API do desafio anterior, você deve criar o front-end, com aspecto de app(iremos chamar de app), usando seus frameworks de preferência._
_Você terá que criar um app baseado em seu Design System de preferência, recomendamos o Material Design do Google, esse app deve conter 3 abas que irão agrupar os gastos das seguintes formas: Lista Geral, Por Mês e Por Categoria._


### Requisitos
- _Todos os requisitos foram implementados conforme as instruções_
- O framework escolhido foi __Angular 9__
- Também foi utilizado novamente como design system o __Angular Material__
- A tela do app foi seguida exatamente como no [exemplo](https://gitlab.com/desafio3/desafio-final/-/blob/master/Experiencia/Experiencia.mov) apresentado.

### Para acessar o projeto
- Clone ou faça download esse projeto
- Dentro da pasta raiz do projeto, rode o comando `npm install` para instalar as dependências
- Rode o comando `ng serve` para inicializar o projeto no endereço `localhost:4200`.
- [__Link__](https://desafio3-brunagil.netlify.com/) do projeto online (deploy via netlify)


### Lógica de implementação
- Como a lógica da consolidação dos gastos por mês já estava implementada no desafio 2, a consolidação pela categoria seguiu o mesmo caminho
- Não foi necessário utilizar o endpoint `categorias` visto que a lógica de implementação tem como base apenas o endpoint `lancamentos`
- Há espaço para evolução da lógica de consolidação ser dinâmico pensando em todos os componentes que a utilizam

__Mais uma vez, obrigada pela oportunidade e por ter acompanhado esse desafio até aqui. 
Qualquer dúvida, estou a disposição!__

